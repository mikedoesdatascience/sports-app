import { MongoClient, ObjectId } from 'mongodb'
import { MONGODB_USERNAME, MONGODB_PASSWORD, MONGODB_URI} from "$env/static/private"

const client = new MongoClient(`mongodb+srv://${MONGODB_USERNAME}:${MONGODB_PASSWORD}@${MONGODB_URI.replace('mongodb+srv://', '')}/?retryWrites=true&w=majority`)
await client.connect()

export { client }
