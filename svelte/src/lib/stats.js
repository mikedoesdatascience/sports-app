const STATS_COLUMNS = ['PTS', 'FGM', 'FGA', 'FG3M', 'FG3A', 'FTM', 'FTA', 'OREB', 'DREB', 'AST', 'STL', 'BLK', 'TOV', 'PF']
const OPP_STATS_COLUMNS = STATS_COLUMNS.map((s) => s + '_OPP')

export { STATS_COLUMNS, OPP_STATS_COLUMNS}