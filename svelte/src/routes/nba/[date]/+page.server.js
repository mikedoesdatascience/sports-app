import { MongoClient, ObjectId } from 'mongodb'
import { client } from "$lib/mongo.js";
import { STATS_COLUMNS, OPP_STATS_COLUMNS } from '$lib/stats.js';

export async function load({ params }) {
    let date = params.date;
    let games = await client.db('nba').collection('games').find({'GAME_DATE': date}).toArray()
    let game = await client.db('nba').collection('games').findOne({'GAME_DATE': date})
    let stats = await client.db('nba').collection('games').aggregate([
        {"$match": {
            'SEASON_ID': game.SEASON_ID,
            'GAME_DATE': {'$lt': params.date},
            'GAME_ID': {'$regex': '^0'}
        }},
        {"$group": {
            "_id": "$TEAM_ABBREVIATION", "GP": {"$count": {}},
            ...STATS_COLUMNS.reduce((a, v) => ({...a, [v]: {'$avg': `$${v}`}}), {}),
            ...OPP_STATS_COLUMNS.reduce((a, v) => ({...a, [v]: {'$avg': `$${v}`}}), {})
        }}
    ]).toArray()
    let tmp = stats.reduce((obj, item) => {
        obj[item._id] = {
            GP: item.GP,
            stats: Object.keys(item).filter((key) => STATS_COLUMNS.includes(key)).reduce((obj, key) => {
                return Object.assign(obj, {
                    [key]: item[key]
                })
            }, {}),
            oppStats: Object.keys(item).filter((key) => OPP_STATS_COLUMNS.includes(key)).reduce((obj, key) => {
                console.log(key)
                return Object.assign(obj, {
                    [key.replace('_OPP', '')]: item[key]
                })
            }, {})
        }
        return obj
    }, {})
    console.log(tmp)
    return {date: date, games: games, stats: stats}
  }
  